package com.pikulyk.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Write your generic class – ship with droids. You can
//put and get droids from ship. Try to implement it using
//wildcards.
//
//Write your PriorityQueue class for droids using
//generics.

public class Ship<T extends Droid> {
  private List<Droid> droids;

  public Ship() {
    this.droids = new ArrayList<>();
  }

  public <E extends Droid> void put(E element) {
    droids.add(element);
  }

  public Droid get(int i) {
    return droids.get(i);
  }

  public void putAll(List<? extends Droid> list) {
    for (Droid droid : list) {
      Collections.addAll(this.droids, droid);
    }
  }

  public List<? extends Droid> getAll() {
    return droids;
  }

  public void showAll() {
    for (Droid droid : droids) {
      System.out.println(droid + " ");
    }
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
