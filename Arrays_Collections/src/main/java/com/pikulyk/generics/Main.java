package com.pikulyk.generics;

public class Main {

  public static void main(String[] args) {
    Ship<Droid> ship = new Ship<>();

    Droid simpleHealer = new HealerDroid(20, "R2D2", 100);
    ship.put(simpleHealer);
    ship.put(new HealerDroid(5, "c3505", 35));
    ship.showAll();

  }
}
