package com.pikulyk.generics;

public class HealerDroid extends Droid {
  private int hpToHeal;

  public HealerDroid(int hpToHheal, String name, int health) {
    this.hpToHeal = hpToHheal;
    setName(name);
    setHealth(health);
  }

  public void heal() {
    setHealth(getHealth() + hpToHeal);
  }

  public void setName(String name) {
    super.setName(name);
  }

  public String getName() {
    return super.getName();
  }

  public void setHealth(int hp) {
    super.setHealth(hp);
  }

  public int getHealth() {
    return super.getHealth();
  }

  @Override
  public String toString() {
    return super.toString() + "\nHeals " + hpToHeal + " health";
  }
}
