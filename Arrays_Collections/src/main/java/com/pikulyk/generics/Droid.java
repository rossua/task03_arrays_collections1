package com.pikulyk.generics;

public abstract class Droid {
  private int health;
  private String name;

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Name: " + this.name
        + "\nHealth = " + this.health;
  }
}
