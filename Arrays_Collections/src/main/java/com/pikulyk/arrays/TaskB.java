package com.pikulyk.arrays;

public class TaskB {

  public static int[] deleteDuplicates(int[] arr) {
    int[] result = new int[0];
    int counter;

    for (int element : arr) {
      counter = 0;
      for (int i = 0; i < arr.length; i++) {
        if (element == arr[i]) {
          counter++;
        }
      }
      if (counter <= 2) {
          result = TaskA.addElementIntoArray(element, result);
        }
    }
    return result;
  }
}
