package com.pikulyk.arrays.game;

import java.util.Random;

public class Door {
  // if true - create a monster , else - artifact
  private boolean isMonster;
  private int points;

  public Door() {
    Random random = new Random();
    this.isMonster = random.nextBoolean();

    if (isMonster) {
      this.points = -random.nextInt(96) + 5;
    } else {
      this.points = random.nextInt(71) + 10;
    }
  }

  public Door(boolean isMonster, int points) {
    this.isMonster = isMonster;
    this.points = points;
  }

  public boolean isMonster() {
    return isMonster;
  }

  public void setMonster(boolean monster) {
    isMonster = monster;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }

  public String  info() {
    if (isMonster) {
      return "Monster with " + this.getPoints() + " points ";
    } else {
      return "Artifact with  " + this.getPoints() + " points";
    }
  }
}
