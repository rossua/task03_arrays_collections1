package com.pikulyk.arrays.game;

public class Hero {
  private int points;
//  private int position;

  public Hero() {
    this.points = 25;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int points) {
    this.points = points;
  }

  public void  info() {
    System.out.println("Your points = " + getPoints());
  }
}
