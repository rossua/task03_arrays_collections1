package com.pikulyk.arrays.game;

import java.util.Scanner;

public class Game {
  private Door[] doors;
  private Hero hero;
  Scanner scanner = new Scanner(System.in);

  public Game() {
    this.doors = new Door[10];
    this.hero = new Hero();
  }

  private void fillDoors() {

    System.out.println("How would you like to fill doors?" +
        "\n1)Randomly" + "\n2)Manually");

    int option = scanner.nextInt();

    switch (option) {
      case 1:
        randomlyFillDoors();
        break;
      case 2:
        manuallyFillDoors();
        break;
    }

    System.out.println("Doors: ");
    for(int i = 0; i < doors.length; i++) {
      System.out.println("| Door " + (i + 1) + " | " + doors[i].info() + " | ");
    }
  }

  private void randomlyFillDoors() {
    for (int i = 0; i < doors.length; i++) {
      doors[i] = new Door();
    }
  }

  private void manuallyFillDoors() {
    boolean isMonster;

    for (int i = 0; i < doors.length; i++) {
      isMonster = chooseMonsterOrArtifact();
      System.out.print("Enter amount of points:"
          + "\n5 - 100 for monster's strength"
          + "\n10 - 80 for artifact's buff\n");
      if(isMonster) {
        doors[i] = new Door(true, scanner.nextInt());
      } else {
        doors[i] = new Door(false, scanner.nextInt());
      }
    }
  }

  private int deadCounter(int doorNumber) {
    if (doorNumber == doors.length) {
      return 0;
    }

    if(doors[doorNumber].getPoints() + hero.getPoints() < 0) {
      return (1 + deadCounter(++doorNumber));
    } else {
      return deadCounter(++doorNumber);
    }
  }

  private boolean chooseMonsterOrArtifact() {

    while(!scanner.hasNextBoolean()) {
      System.out.print("Is monster or artifact behind this door?" +
          " (true = monster or false = artifact)\n");
      scanner.next();
    }

    boolean isEnemy = scanner.nextBoolean();
    return isEnemy;
  }

  public void startGame() {
    int imput;

    for (int i = 0; i < doors.length; i++) {
      if (hero.getPoints() < 0) {
        System.out.println("You lose!");
        break;
      } else {
        System.out.println("Choose the door: ");
        imput = scanner.nextInt();
        if (doors[imput - 1] != null){
          hero.setPoints(hero.getPoints() + doors[imput - 1].getPoints());
          hero.info();
          doors[imput - 1] = null;
        } else {
          System.out.println("This door is already opened, choose another one:");
          i--;
        }
      }
    }
    if (hero.getPoints() >= 0) {
      System.out.println("You win!");
    }
  }

  public void menu() {
    int userChoice;

    fillDoors();
    System.out.println("Enter your choice: \n"
        + "1)Dead counter\n"
        + "2)Win condition\n"
        + "3)Exit");
    userChoice = scanner.nextInt();
    switch(userChoice) {
      case 1:
        System.out.println(deadCounter(0));
        break;
      case 2:
        System.out.println(" ");
        break;
      case 3:
        System.out.println("Thank you :)");
        return;
    }
    startGame();
  }

  public static void main(String[] args) {
    Game game = new Game();
    game.menu();
  }
}
