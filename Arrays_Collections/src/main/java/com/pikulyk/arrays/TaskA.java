package com.pikulyk.arrays;

public class TaskA {

  public static int[] addElementIntoArray(int element, int[] arr) {
    int[] result = increaseArrayByOne(arr);
    result[lastIndex(result)] = element;
    return result;
  }

  private static int[] increaseArrayByOne(int[] source) {
    int[] result = new int[source.length + 1];
    for (int i = 0; i < source.length; i++) {
      result[i] = source[i];
    }
    return result;
  }

  private static int lastIndex(int[] arr) {
    return arr.length - 1;
  }

  public static int[] getArrContainsElementsInBoth(int[] arrayOne, int[] arrayTwo) {
    int[] result = new int[0];

    for (int i = 0; i < arrayOne.length; i++) {
      for (int j = 0; j < arrayTwo.length; j++) {
        if (arrayOne[i] == arrayTwo[j]) {
          if (!alreadyAdded(arrayOne[i], result)) {
            result = addElementIntoArray(arrayOne[i], result);
          }
        }
      }
    }
    return result;
  }

  public static int[] getArrContainsElementsInOne(int[] arrayOne, int[] arrayTwo) {
    int[] result = new int[0];

    for (int i = 0; i < arrayOne.length; i++) {
      for (int j = 0; j < arrayTwo.length; j++) {
        if (arrayOne[i] == arrayTwo[j]) {
          break;
        }
        if (j == lastIndex(arrayTwo)) {
          if (!alreadyAdded(arrayOne[i], result)) {
            result = addElementIntoArray(arrayOne[i], result);
          }
        }
      }
    }
//    for (int i = 0; i < arrayTwo.length; i++) {
//      for (int j = 0; j < arrayOne.length; j++) {
//        if (arrayTwo[i] == arrayOne[j]) {
//          break;
//        }
//        if (j == lastIndex(arrayOne)) {
//          if (!alreadyAdded(arrayTwo[i], result)) {
//            result = addElementIntoArray(arrayTwo[i], result);
//          }
//        }
//      }
//    }
    return result;
  }

  private static boolean alreadyAdded(int element, int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      if (arr[i] == element) {
        return true;
      }
    }
    return false;
  }
}
