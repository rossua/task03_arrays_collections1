package com.pikulyk.arrays;

//Task :  Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з них всі елементи крім одного.

public class TaskC {

  public static int[] deleteSeriesOfSameElements(int[] arr) {
    int[] result = new int[0];

    for (int i = 0; i < arr.length; i++) {
      if (i != (arr.length - 1)) {
       if (arr[i] != arr[i + 1]) {
         result = TaskA.addElementIntoArray(arr[i], result);
       }
      } else {
        result = TaskA.addElementIntoArray(arr[i], result);
      }
    }

    return result;
  }
}
