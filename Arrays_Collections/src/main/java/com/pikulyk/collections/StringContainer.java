package com.pikulyk.collections;

public class StringContainer {
  private String[] array = new String[7];
  private int size;

  private void rangeCheck(int var1) {
    if (var1 >= this.size) {
      System.out.println("Index is out of bounds of array");
      throw new IndexOutOfBoundsException();
    }
  }

  private boolean capacityCheck() {
    if (size + 1 > array.length) {
      return true;
    } else {
      return false;
    }
  }

  private void increaseArray() {
    String[] tmp = array;
    array = new String[size * 2];
    for (int i = 0; i < tmp.length; i++) {
      array[i] = tmp[i];
    }
  }

  public void add(String value) {
    if (capacityCheck()) {
      increaseArray();
    }
    this.array[this.size] = value;
    size++;
  }

  public String get(int index){
    rangeCheck(index);
    return this.array[index];
  }

  public int size() {
    return this.size;
  }

}
