package com.pikulyk.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;

public class DeString implements Comparable<DeString> {
  private String first;
  private String second;

  public DeString(String first, String second) {
    this.first = first;
    this.second = second;
  }

  public String getFirst() {
    return first;
  }

  public void setFirst(String first) {
    this.first = first;
  }

  public String getSecond() {
    return second;
  }

  public void setSecond(String second) {
    this.second = second;
  }

  @Override
  public int compareTo(DeString obj) {
    if(this.first.length() > obj.first.length()) {
      return 1;
    } else if(this.first.length() < obj.first.length()) {
      return -1;
    } else {
      return 0;
    }
  }

  @Override
  public String toString() {
    return first + " " + second;
  }

  public static void main(String[] args) {
    List<DeString> deStringList = new ArrayList<>();
    deStringList.add(new DeString("Ukraine", "Lviv"));
    deStringList.add(new DeString("Ukraine", "Kyiv"));
    deStringList.add(new DeString("Poland", "Krakov"));
    deStringList.add(new DeString("England", "London"));
    deStringList.add(new DeString("Italy", "Rome"));
    deStringList.add(new DeString("Germany", "Berlin"));

    Collections.sort(deStringList);

    for (int i = 0; i < deStringList.size(); i++) {
      System.out.println(deStringList.get(i).toString());
    }

    DeString searchObj = new DeString("England", "London");
    int indexOfSearchObj = Collections.binarySearch(deStringList, searchObj);

    System.out.println("\nBinary search example:");
    System.out.println("Index of object " + searchObj.toString() + " = " + indexOfSearchObj );
  }


}
