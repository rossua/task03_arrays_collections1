package com.pikulyk.collections;


import java.util.ArrayList;
import java.util.List;


public class Main {

  public static void main(String[] args) {
    StringContainer str = new StringContainer();
    List<String> strL = new ArrayList<>();

    long timeMillis = System.currentTimeMillis();
    str.add("a");
    str.add("b");
    str.add("c");
    str.add("d");
    str.add("e");
    str.add("f");
    str.add("g");
    str.add("h");
    str.add("i");
    str.add("j");
    str.add("k");
    str.add("l");
    str.add("m");
    System.out.println(System.currentTimeMillis() - timeMillis);
    for (int i = 0; i < str.size(); i++) {
      System.out.print(str.get(i) + " ");
    }
    System.out.println("\n" + str.get(13));

    strL.add("a");
    strL.add("b");
    strL.add("c");
    strL.add("d");
    strL.add("e");
    strL.add("f");
    strL.add("g");
    strL.add("h");
    strL.add("i");
    strL.add("j");
    strL.add("k");
    strL.add("l");
    strL.add("m");
    System.out.println(System.currentTimeMillis() - timeMillis);
    for (int i = 0; i < strL.size(); i++) {
      System.out.print(strL.get(i) + " ");
    }
  }


}
